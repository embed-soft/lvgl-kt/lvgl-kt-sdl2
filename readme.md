# LVGL KT SDL 2 (lvglkt-sdl2)

A Kotlin Native library that provides Kotlin bindings to the SDL 2 (display backend) part of the 
[LV Drivers library](https://github.com/lvgl/lv_drivers). This library depends on Kotlin Native (currently in beta), 
and [LVGL KT Drivers](https://gitlab.com/embed-soft/lvgl-kt/lvgl-kt-drivers). Below is the key functionality provided 
by this library:

- Keyboard
- Mouse
- Display
- Basic access to SDL 2 display backend

The following Kotlin Native targets are supported:
- linuxX64


## Requirements

The following is required by the library in order to use it:

- Kotlin Native 1.7.21 or later
- Gradle 6.9.2 or later
- LV Drivers 8.3

Note that the **liblv_drivers.a** file (a static library file) **MUST** be generated for a Kotlin Native program using
this library, from the LV Drivers source code via [cmake](https://cmake.org/). Alternatively an existing
**liblv_drivers.a** file can be acquired from the
[Counter sample](https://gitlab.com/embed-soft/lvgl-kt/samples/counter-sample/-/tree/master/lib).


## Usage

To use this library in a Kotlin Native program using Gradle add the library as a dependency. Add the following to
the build file: `implementation(io.gitlab.embed-soft:lvglkt-sdl2:0.4.0)`

In order to use the SDL 2 display backend initialize its HAL (Hardware Abstraction Layer) via the 
[SDL2 object](src/commonMain/kotlin/io.gitlab.embedSoft.lvglKt.sdl2/Sdl2.kt), eg:
```kotlin
// ...
Sdl2.initHal(enableDblBuffer = true)
```


## Input Devices

This library provides support for input devices via SDL 2. Some input devices that are supported include the 
following:

- Mouse
- Mouse Wheel
- Keyboard

To use the Mouse Wheel call the `setupMouseWheel` function. With the Keyboard call the `seupKeyboard` function. All the 
functions come from [ioUtils.kt](src/commonMain/kotlin/io.gitlab.embedSoft.lvglKt.sdl2/ioUtils.kt). Remember to 
call the `Sdl2.initHal` function to initialize/setup all input devices first **before** using them.
