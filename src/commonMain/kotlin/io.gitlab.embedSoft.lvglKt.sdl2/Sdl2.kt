package io.gitlab.embedSoft.lvglKt.sdl2

import io.gitlab.embedSoft.lvglKt.core.Area
import io.gitlab.embedSoft.lvglKt.core.display.DisplayDrawBuffer
import io.gitlab.embedSoft.lvglKt.core.display.DisplayDriver
import io.gitlab.embedSoft.lvglKt.core.styling.Color
import io.gitlab.embedSoft.lvglKt.drivers.Display

/** Covers all functionality with the SDL 2 display backend. */
public expect object Sdl2 {
    /**
     * Initializes the HAL (Hardware Abstraction Layer) for LVGL. It is **strongly** recommended that this function is
     * used since it automates many tasks. Remember to call [io.gitlab.embedSoft.lvglKt.core.initLvgl] **before** using
     * this function.
     * @param enableDblBuffer If *true* then double buffering is used.
     * @return A Triple containing the following:
     * 1. Display buffer
     * 2. Display driver
     * 3. Display
     */
    public fun initHal(enableDblBuffer: Boolean): Triple<DisplayDrawBuffer, DisplayDriver, Display>

    /**
     * Creates a display (includes the display buffer and driver) for output via the SDL 2 backend.
     * @param enableDblBuffer If *true* then double buffering is used.
     * @return A Triple the contains the following:
     * 1. Display buffer
     * 2. Display driver
     * 3. Display
     */
    public fun createDisplay(enableDblBuffer: Boolean): Triple<DisplayDrawBuffer, DisplayDriver, Display>

    /**
     * Creates the display driver that uses the SDL 2 display backend.
     * @param drawBuf The draw buffer to use.
     * @param init The lambda that initializes the object.
     * @return The new [DisplayDriver].
     */
    public fun createDisplayDriver(drawBuf: DisplayDrawBuffer, init: DisplayDriver.() -> Unit): DisplayDriver

    /**
     * Refreshes the display via SDL 2.
     * @param displayDriver The display driver to use.
     * @param area The area to use.
     * @param color The color to use.
     */
    public fun flush(displayDriver: DisplayDriver, area: Area, color: Color)
}
