package io.gitlab.embedSoft.lvglKt.sdl2

import io.gitlab.embedSoft.lvglKt.drivers.input.InputDevice
import io.gitlab.embedSoft.lvglKt.drivers.input.InputDeviceDriver

/** Sets up the Keyboard as an input device. */
public expect fun setupKeyboard(): Pair<InputDeviceDriver, InputDevice>

/** Sets up the Mouse Wheel as an input device.*/
public expect fun setupMouseWheel(): Pair<InputDeviceDriver, InputDevice>
