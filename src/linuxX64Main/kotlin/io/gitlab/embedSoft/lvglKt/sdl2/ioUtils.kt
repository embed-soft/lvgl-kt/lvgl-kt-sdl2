package io.gitlab.embedSoft.lvglKt.sdl2

import io.gitlab.embedSoft.lvglKt.drivers.input.InputDevice
import io.gitlab.embedSoft.lvglKt.drivers.input.InputDeviceDriver
import io.gitlab.embedSoft.lvglKt.drivers.input.InputDeviceType
import io.gitlab.embedSoft.lvglKt.sdl2.clib.lvDrivers.*

public actual fun setupKeyboard(): Pair<InputDeviceDriver, InputDevice> {
    val inDevDriver = InputDeviceDriver.create()
    inDevDriver.type = InputDeviceType.KEYPAD
    inDevDriver.onRead = { driver, data -> sdl_keyboard_read(driver.lvIndevDrvPtr, data.lvIndevDataPtr) }
    // Register the input device so it can be used.
    val keyboardInDev = inDevDriver.register()
    return inDevDriver to keyboardInDev
}

public actual fun setupMouseWheel(): Pair<InputDeviceDriver, InputDevice> {
    val inDevDriver = InputDeviceDriver.create()
    inDevDriver.type = InputDeviceType.ENCODER
    inDevDriver.onRead = { driver, data -> sdl_mousewheel_read(driver.lvIndevDrvPtr, data.lvIndevDataPtr) }
    // Register the input device so it can be used.
    val mouseWheelInDev = inDevDriver.register()
    return inDevDriver to mouseWheelInDev
}
